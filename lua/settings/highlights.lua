local c = require("settings.colors").Color()
local d = require("settings.colors").Default()

vim.cmd("hi DiagnosticVirtualTextError guifg=Red guibg=" .. d.normal2)
vim.cmd("hi DiagnosticVirtualTextWarn guifg=Orange guibg=" .. d.normal2)
vim.cmd("hi DiagnosticVirtualTextInfo guifg=Magenta guibg=" .. d.normal2)
vim.cmd("hi DiagnosticVirtualTextHint guifg=#8ec07c guibg=" .. d.normal2)

vim.cmd('hi DiagnosticSignError guifg=Red guibg=NONE')
vim.cmd('sign define DiagnosticSignError text= texthl=DiagnosticSignError')

vim.cmd('hi DiagnosticSignWarn guifg=Orange guibg=NONE')
vim.cmd('sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn')

vim.cmd('hi DiagnosticSignInfo guifg=Magenta guibg=NONE')
vim.cmd('sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo')

vim.cmd('hi DiagnosticSignHint guifg=#8ec07c guibg=NONE')
vim.cmd('sign define DiagnosticSignHint text= texthl=DiagnosticSignHint')

vim.cmd("hi CursorLine guibg=#303030")
vim.cmd("hi SignColumn guibg=none")
vim.cmd("hi GitSignsAdd guifg=" .. c.git_add .. " guibg=none")
vim.cmd("hi GitSignsChange guifg=" .. c.git_change ..  " guibg=none")
vim.cmd("hi GitSignsDelete guifg=" .. c.git_del .. " guibg=none")
vim.cmd("hi GitSignsCurrentLineBlame guifg=" .. c.gray6b)

vim.cmd("hi! Normal guibg=" .. d.normal)
vim.cmd("hi! NormalNC guibg=" .. d.normal)
vim.cmd("hi! NormalFloat guibg=" .. d.normal)
vim.cmd("hi! FloatBorder guibg=none guifg=" .. d.text)
vim.cmd("hi! MsgArea guibg=" .. d.normal)
vim.cmd("hi! ModeMsg guibg=" .. d.normal)
vim.cmd("hi! ErrorMsg guibg=" .. d.normal)
vim.cmd("hi! WarningMsg guibg=" .. d.normal)
vim.cmd("hi! VertSplit guibg=none")
vim.cmd("hi! WinSeparator guibg=" .. d.bg)
vim.cmd("hi! EndOfBuffer guifg=" .. c.gray .. " guibg=" .. d.normal)

vim.cmd("hi! WinBar guibg=" .. d.bg)
vim.cmd("hi! WinBarNC guibg=" .. d.bg)
vim.cmd("hi! StatusLine guibg=" .. d.bg .. " guifg=" .. d.text .. " gui=none cterm=none")
vim.cmd("hi! StatusLineNC guibg=" .. d.bg .. " guifg=" .. d.textNC .. " gui=none cterm=none")
vim.cmd("hi! TabLine guibg=" .. d.bg )
vim.cmd("hi! TabLineFill cterm=none gui=none guibg=" .. d.bg .. " guifg=" .. d.textNC)

vim.cmd("hi! NvimTreeNormal guibg=" .. d.bg .. " guifg=none")
vim.cmd("hi! NvimTreeNormalNC guibg=" .. d.bg)
vim.cmd("hi! NvimTreeEndOfBuffer guifg=" .. c.gray .. " guibg=" .. d.bg)
vim.cmd("hi! NvimTreeWinSeparator guibg=" .. d.bg)

vim.cmd("hi! Search guifg=#282828 guibg=#fabd2f")

vim.cmd("hi! TreesitterContext gui=bold guibg=#474947")
vim.cmd("hi! TreesitterContextLineNumber gui=bold guifg=" .. d.textNC .. " guibg=#474947")
vim.cmd("hi! TreesitterContextBottom gui=bold guibg=#353735")

vim.cmd("hi! EyelinerPrimary guifg=#00ffff gui=bold")
vim.cmd("hi! EyelinerSecondary guifg=#ff00ff gui=bold")
vim.cmd("hi! TelescopeNormal guibg=none")

vim.cmd("hi! link RenameNormal NormalFloat")
vim.cmd("hi! link SagaBorder FloatBorder")

vim.cmd("hi! IndentBlankLineChar guifg=".. c.gray2a)
vim.cmd("hi! IndentBlankLineContextChar guifg=#569cd6")

vim.cmd[[
highlight! CmpItemAbbrDeprecated guibg=NONE gui=strikethrough guifg=#808080
highlight! CmpItemAbbrMatch guibg=NONE guifg=#569CD6
highlight! link CmpItemAbbrMatchFuzzy CmpItemAbbrMatch
highlight! CmpItemKindSnippet guibg=NONE guifg=#fadd2f
]]
