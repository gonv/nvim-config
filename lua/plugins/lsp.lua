return {
  'neovim/nvim-lspconfig',
  dependencies = {
    'williamboman/mason.nvim',
    'williamboman/mason-lspconfig.nvim',
    'j-hui/fidget.nvim',
    'folke/neodev.nvim',
  },
  config = function()
    local lspconfig = require("lspconfig");
    vim.diagnostic.config({
      update_in_insert = true,
      -- signs = {
      --   severity = { min = vim.diagnostic.severity.WARN }
      -- },
      virtual_text = {
        prefix = '●',
        source = "if_many"
      },
    })

    local max_width = math.max(math.floor(vim.o.columns * 0.7), 100)
    local max_height = math.max(math.floor(vim.o.lines * 0.3), 30)

    vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
      border = 'rounded',
      max_width = max_width,
      max_height = max_height,
    })

    vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
      border = 'rounded',
      max_width = max_width,
      max_height = max_height,
    })

    -- LSP settings.
    --  This function gets run when an LSP connects to a particular buffer.
    local on_attach = function(_, bufnr)
      local navic = require("nvim-navic")

      if _.server_capabilities.documentSymbolProvider then
        navic.attach(_, bufnr)
      end

    end

    -- Enable the following language servers
    --  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
    --
    --  Add any additional override configuration in the following tables. They will be passed to
    --  the `settings` field of the server config. You must look up that documentation yourself.
    local servers = {
      bashls = {},
      astro = {},
      cssls = {},
      html = {},
      jsonls = {},
      sumneko_lua = {
        Lua = {
          workspace = { checkThirdParty = false },
          telemetry = { enable = false },
        },
      },
      marksman = {},
      svelte = {},
      tailwindcss = {},
      tsserver = {},
    }

    -- Setup neovim lua configuration
    require('neodev').setup()

    -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

    -- Setup mason so it can manage external tooling
    require('mason').setup({
      ui = {
        check_outdated_packages_on_open = true,
        border = "rounded",
        icons = {
          package_installed = "🗹",
          package_pending = "🞔",
          package_uninstalled = "□",
        },
      }
    })

    -- Ensure the servers above are installed
    local mason_lspconfig = require 'mason-lspconfig'

    mason_lspconfig.setup {
      ensure_installed = vim.tbl_keys(servers),
    }

    mason_lspconfig.setup_handlers {
      function(server_name)
        lspconfig[server_name].setup {
          capabilities = capabilities,
          on_attach = on_attach,
          settings = servers[server_name],
        }
      end,
    }

    require('fidget').setup()
  end
}
