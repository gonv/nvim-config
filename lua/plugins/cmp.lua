return {
  'hrsh7th/nvim-cmp',
  dependencies = {
    'hrsh7th/cmp-nvim-lsp',
    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',
    'onsails/lspkind.nvim',
    'rafamadriz/friendly-snippets'
  },
  config = function()
    local cmp = require 'cmp'
    local luasnip = require 'luasnip'
    local lspkind = require 'lspkind'

    local source_mapping = {
      buffer = "Bfr",
      nvim_lsp = "LSP",
      luasnip = "Snp",
      cmp_tabnine = "TbN",
      path = "Pth",
    }

    cmp.setup {
      snippet = {
        expand = function(args)
          luasnip.lsp_expand(args.body)
        end,
      },
      mapping = cmp.mapping.preset.insert {
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<CR>'] = cmp.mapping.confirm {
          behavior = cmp.ConfirmBehavior.Replace,
          select = true,
        },
        ['<Tab>'] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_next_item()
          elseif luasnip.expand_or_jumpable() then
            luasnip.expand_or_jump()
          else
            fallback()
          end
        end, { 'i', 's' }),
        ['<S-Tab>'] = cmp.mapping(function(fallback)
          if cmp.visible() then
            cmp.select_prev_item()
          elseif luasnip.jumpable(-1) then
            luasnip.jump(-1)
          else
            fallback()
          end
        end, { 'i', 's' }),
      },
      sources = {
        { name = 'buffer' },
        { name = 'cmp_tabnine' },
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'path' },
      },
      window = {
        completion = {
          winhighlight = "Normal:FloatNormal,FloatBorder:FloatBorder,Search:None",
          col_offset = -2,
          side_padding = 0,
          border = 'rounded'
        },
        documentation = {
          winhighlight = "Normal:FloatNormal,FloatBorder:FloatBorder,Search:None",
          border = 'rounded'
        }
      },
      formatting = {
        fields = { "kind", "abbr", "menu" },
        format = function(entry, vim_item)
          -- Show menu text from lspkind
          local kind = lspkind.cmp_format({ mode = "symbol_text", maxwidth = 50 })(entry, vim_item)
          local strings = vim.split(kind.kind, "%s", { trimempty = true })
          kind.kind = " " .. (strings[1] or "") .. " "
          kind.menu = " " .. (strings[2] or "")

          -- Show menu text from source_mapping var
          -- vim_item.kind = lspkind.symbolic(vim_item.kind, { mode = "symbol" })
          -- vim_item.menu = ' ' .. source_mapping[entry.source.name]

          if entry.source.name == "cmp_tabnine" then
            vim_item.kind = " " .. "" .. " "
            local detail = (entry.completion_item.data or {}).detail
            if detail and detail:find('.*%%.*') then
              vim_item.menu = '   ' .. detail
            end
          end
          local maxwidth = 50
          vim_item.abbr = string.sub(vim_item.abbr, 1, maxwidth)
          return vim_item
        end,
      }
    }

    -- Turn on Snippets and config
    require("luasnip.loaders.from_vscode").lazy_load()
    require("luasnip").filetype_extend("svelte", { "html" })

    -- The line beneath this is called `modeline`. See `:help modeline`
    -- vim: ts=2 sts=2 sw=2 et

  end
}
