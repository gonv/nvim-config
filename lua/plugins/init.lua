return {
  { "lunarvim/darkplus.nvim",
    priority = 1000,
    config = function()
      vim.cmd("colorscheme darkplus")
    end
  },

  { "SmiteshP/nvim-navic",
    dependencies = "neovim/nvim-lspconfig"
  },

  { 'windwp/nvim-autopairs',
    config = function()
      require("nvim-autopairs").setup {}
    end
  },

  { "lazytanuki/nvim-mapper",
    dependencies = { "telescope.nvim" },
    config = function()
      local mapper = require("nvim-mapper")
      mapper.setup({
        no_map = true,
      })
    end
  },

  'kdheepak/lazygit.nvim',

  { 'numToStr/Comment.nvim',
    config = function()
      require("Comment").setup()
    end
  },

  { "iamcco/markdown-preview.nvim",
    build = function() vim.fn["mkdp#util#install"]()
    end
  },

  { 'nvim-telescope/telescope-fzf-native.nvim',
    build = 'make',
    cond = vim.fn.executable 'make' == 1
  },

  'MattesGroeger/vim-bookmarks',

  'mbbill/undotree',

  'stevearc/dressing.nvim',

  'j-morano/buffer_manager.nvim',

  'famiu/bufdelete.nvim',

  { 'norcalli/nvim-colorizer.lua',
    config = function()
      require("colorizer").setup({
        '*';
      })
    end
  },

  { 'jinh0/eyeliner.nvim',
    config = function()
      require("eyeliner").setup({
        highlight_on_key = true
      })
    end
  },

  { 'dstein64/nvim-scrollview',
    config = function()
      require("scrollview").setup({
        column = 1
      })
    end
  },

  { "danymat/neogen",
    dependencies = "nvim-treesitter/nvim-treesitter",
    version = "*",
    config = function()
      require("neogen").setup({
        snippet_engine = "luasnip"
      })
    end
  },

  { "folke/trouble.nvim",
    dependencies = "nvim-tree/nvim-web-devicons",
    config = function()
      require("trouble").setup {}
    end
  },

  { 'rcarriga/nvim-notify',
    config = function()
      require('notify').setup({
        stages = "slide",
      })
    end
  },
}
